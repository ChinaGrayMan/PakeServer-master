/** 全局变量 */
pageNum=1;
pageSize=10;
var rotation = function() {
    $("#goTopIcon").rotate({
        angle:0,
        duration: 3000,
        animateTo: 1080,
        callback:function(){
            $("#goTopIcon").html('<i class="material-icons md-24 text-primary"></i>');
        }
    });
};

/*
 * 注意：
 * 1. 所有的JS接口只能在公众号绑定的域名下调用，公众号开发者需要先登录微信公众平台进入“公众号设置”的“功能设置”里填写“JS接口安全域名”。
 * 2. 如果发现在 Android 不能分享自定义内容，请到官网下载最新的包覆盖安装，Android 自定义分享接口需升级至 6.0.2.58 版本及以上。
 * 3. 常见问题及完整 JS-SDK 文档地址：http://mp.weixin.qq.com/wiki/7/aaa137b55fb2e0456bf8dd9148dd613f.html
 */

$(function () {
    $("#goTopIcon").html('<i class="material-icons md-24 text-primary"></i>');
    //初始化微信 JS SDK
    var url = location.href.split('#')[0];
    var title = $(document).attr("title");
    var desc = '一拍就送，派客配送';
    var imgUrl = 'www.wanxunkj.com/resource/wechat/images/logo_black.png';
    $.ajax({
        url: '/weChatApi/jsTicket',
        type: 'post',
        data: {url: url},
        dataType: 'json',
        success: function (data) {
            if(data.code==0){
                wx.config({
                    debug: false,
                    appId: data.data.appId,
                    timestamp: data.data.timestamp,
                    nonceStr: data.data.nonceStr,
                    signature: data.data.signature,
                    jsApiList: [
                        'checkJsApi',
                        'onMenuShareTimeline',
                        'onMenuShareAppMessage',
                        'onMenuShareQQ',
                        'onMenuShareWeibo',
                        'onMenuShareQZone',
                        'hideMenuItems',
                        'showMenuItems',
                        'hideAllNonBaseMenuItem',
                        'showAllNonBaseMenuItem',
                        'translateVoice',
                        'startRecord',
                        'stopRecord',
                        'onVoiceRecordEnd',
                        'playVoice',
                        'onVoicePlayEnd',
                        'pauseVoice',
                        'stopVoice',
                        'uploadVoice',
                        'downloadVoice',
                        'chooseImage',
                        'previewImage',
                        'uploadImage',
                        'downloadImage',
                        'getNetworkType',
                        'openLocation',
                        'getLocation',
                        'hideOptionMenu',
                        'showOptionMenu',
                        'closeWindow',
                        'scanQRCode',
                        'chooseWXPay',
                        'openProductSpecificView',
                        'addCard',
                        'chooseCard',
                        'openCard'
                    ]
                });
                wx.ready(function () {
                    //显示右上角菜单
                    wx.showOptionMenu();
                    // 要隐藏的菜单项，只能隐藏“传播类”和“保护类”按钮
                    wx.hideMenuItems({
                        menuList: ['menuItem:editTag', 'menuItem:delete', 'menuItem:copyUrl', 'menuItem:originPage', 'menuItem:readMode', 'menuItem:openWithQQBrowser', 'menuItem:openWithSafari', 'menuItem:share:email', 'menuItem:share:brand', 'menuItem:share:qq', 'menuItem:share:QZone']
                    });
                    // 要显示的菜单项
                    wx.showMenuItems({
                        menuList: ['menuItem:share:appMessage', 'menuItem:share:timeline', 'menuItem:favorite']
                    });
                    //分享到朋友圈
                    wx.onMenuShareTimeline({
                        title: title,
                        link: url,
                        imgUrl: imgUrl,
                        success: function () {
                            layer.msg("谢谢您的分享");
                        },
                        cancel: function () {
                            layer.msg("已取消分享");
                        }
                    });
                    //分享给朋友
                    wx.onMenuShareAppMessage({
                        title: title,
                        desc: desc,
                        link: url,
                        imgUrl: imgUrl,
                        type: '',
                        dataUrl: '',
                        success: function () {
                            layer.msg("谢谢您的分享");
                        },
                        cancel: function () {
                            layer.msg("已取消分享");
                        }
                    });
                });

            }else {
                layer.msg(data.msg);
            }
        }
    });
});



/** 异步全局配置 */
$.ajaxSetup({
    dataType: "json",
    cache: false,
    timeout: 8000,
    xhrFields: {
        withCredentials: true
    },
    error:function (XMLHttpRequest){
        switch (XMLHttpRequest.status){
            case(500):
                $("#body").load("/static/error/500.html");
                break;
            case(404):
                $("#body").load("/static/error/404.html");
                break;
            default:
                $("#body").load("/static/error/error.html");
        }
    }
});
$(document).ajaxSend(
    function () {
    $("#goTopIcon").html('<i class="material-icons md-24 text-primary"></i>');
    rotation();
}).ajaxSuccess(
    function () {
    $("#body").removeClass("loadingIco");
    $("img").lazyload({
        effect: "fadeIn",
        threshold :500,
        placeholder : "/static/images/loading.gif"
    });
});


/** template全局配置 */
/** 图片地址前缀 */
template.helper('url_prefix', function () {
        return "http://www.cdsszwhg.com"
});

/** 计数器 */
var i = 0;
template.helper('sum', function () {
  i++;
  return i;
}); 

/** 返回顶部*/
function goTop(){
$('body,html').animate({scrollTop:0},500);
};

/** 历史地址记录*/
function addressInfo() {
    $.ajax({
        url:"/paKe/user/getHistoryAddress",
        success:function (data) {
            if (!$.isEmptyObject(data)) {
                if(data.code==0){
                    $('#addressUtil').html(template('addressModalTemplate', data));
                    $('#addressModal').modal('show');
                }else{
                    layer.message(data.msg);
                }
            }else{
                $('#addressUtil').html('<li class="list-item" style="text-align: center;">没有更多啦</li>');
            }
        },
        error:function () {
            layer.msg('获取失败');
        }
    });
};

/** 地址选择*/
function addAddress(name,phone,address) {
$("#name").val(name);
$("#phone").val(phone);
$("#address").val(address);
$('#addressModal').modal('hide');
};

//表单序列化成JSON对象
jQuery.prototype.serializeObject=function(){  
    var obj=new Object();  
    $.each(this.serializeArray(),function(index,param){  
        if(!(param.name in obj)){  
            obj[param.name]=param.value;  
        }  
    });  
    return obj;  
};  

//字符串转日期格式
function getDate(strDate) {
	 var date = eval('new Date(' + strDate.replace(/\d+(?=-[^-]+$)/,
	  function (a) { return parseInt(a, 10) - 1; }).match(/\d+/g) + ')');
	 return date;
 }

function checkPhone(phone) {
var pattern = /^1[34578]\d{9}$/;
return pattern.test(phone);
}

/** 滚动显示*/
$(window).scroll(function(){
    var sc=$(window).scrollTop();
    if(sc>100){
        $("#goTop").css("display","block");
    }else{
        $("#goTop").css("display","none");
    }
});
