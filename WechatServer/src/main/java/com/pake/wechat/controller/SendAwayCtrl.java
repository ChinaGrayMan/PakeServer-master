package com.pake.wechat.controller;


import com.pake.wechat.model.Address;
import com.pake.wechat.model.TakeSend;
import com.pake.wechat.model.WeChatFans;
import com.pake.wechat.service.ITakeSendService;
import com.pake.wechat.service.IUserService;
import com.pake.wechat.util.AppUtil;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 寄
 */
@Controller
@RequestMapping("/paKe/sendAway")
public class SendAwayCtrl {

    @Resource
    private ITakeSendService takeSendService;

    @Resource
    private IUserService userService;

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        DateFormat dateFormat=new SimpleDateFormat("yyyy/MM/dd HH:mm");
        binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, true));
    }

    @RequestMapping("/index")
    public String index(Model model, HttpServletRequest request){
        WeChatFans weChatFans = (WeChatFans) request.getSession().getAttribute("loginUser");
        Address address =userService.getNewAddress(weChatFans.getOpenId());
        model.addAttribute("address",address);
        return "sendAway/index";
    }

    /**
     * 系统下单
     */
    @PostMapping("/pleaseOrder")
    public String pleaseOrder(TakeSend takeSend, Model model){
        String orderId = AppUtil.getOrderIdByUUId();
        takeSend.setId(orderId);
        takeSendService.pleaseOrder(takeSend);
        model.addAttribute("orderId",orderId);
        model.addAttribute("type",2);
        return "sendAway/success";
    }
}
