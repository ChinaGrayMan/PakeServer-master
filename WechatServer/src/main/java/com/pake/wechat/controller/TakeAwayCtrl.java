package com.pake.wechat.controller;


import com.pake.wechat.model.Address;
import com.pake.wechat.model.TakeAway;
import com.pake.wechat.model.WeChatFans;
import com.pake.wechat.service.ITakeAwayService;
import com.pake.wechat.service.IUserService;
import com.pake.wechat.util.AppUtil;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 取
 */
@Controller
@RequestMapping("/paKe/takeAway")
public class TakeAwayCtrl {

    @Resource
    private ITakeAwayService takeAwayService;

    @Resource
    private IUserService userService;

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        DateFormat dateFormat=new SimpleDateFormat("yyyy/MM/dd HH:mm");
        binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, true));
    }

    @RequestMapping("/index")
    public String index(Model model, HttpServletRequest request){
        WeChatFans weChatFans = (WeChatFans) request.getSession().getAttribute("loginUser");
        Address address =userService.getNewAddress(weChatFans.getOpenId());
        model.addAttribute("address",address);
        return "takeAway/index";
    }


    /**
     * 系统下单
     */
    @PostMapping(value = "/pleaseOrder")
    @ResponseBody
    @Transactional
    public String pleaseOrder(TakeAway takeAway){
        String orderId = AppUtil.getOrderIdByUUId();
        takeAway.setId(orderId);
        takeAwayService.pleaseOrder(takeAway);
        return orderId;
    }
}
