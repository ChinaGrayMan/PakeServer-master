package com.pake.wechat.controller;


import com.pake.wechat.common.result.Result;
import com.pake.wechat.common.result.ResultCode;
import com.pake.wechat.model.*;
import com.pake.wechat.service.ITakeAwayService;
import com.pake.wechat.service.ITakeSendService;
import com.pake.wechat.service.IUserService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * 用户
 */
@Controller
@RequestMapping("/paKe/user")
public class UserCtrl {

    @Resource
    private IUserService userService;

    @Resource
    private ITakeAwayService takeAwayService;

    @Resource
    private ITakeSendService takeSendService;

    @RequestMapping("/userInfo")
    public String index(){
        return "user/userInfo";
    }


    /**
     * 根据用户OpenId获取订单
     *
     * */
    @GetMapping("/getOrders")
    @ResponseBody
    public Result getOrders(
            @RequestParam(name = "openId")String openId,
            @RequestParam(name = "pageNum", defaultValue = "1") int pageNum,
            @RequestParam(name = "pageSize",defaultValue = "10") int pageSize
    ){
        Result result = new Result();
        List<Order> data = userService.getOrders(openId,pageNum,pageSize);
        result.setResultCode(ResultCode.COMMON_SUCCESS);
        result.setData(data);
        return result;
    }

    /**
     * 根据订单编号获取订单详情
     *
     * */
    @GetMapping("/getOrderInfo")
    @ResponseBody
    public Result getOrderInfo(
            @RequestParam(name = "id") String id,
            @RequestParam(name = "type") int type
    ){
        Result result = new Result();
        switch (type){
            case 1:
                TakeAway takeAway = takeAwayService.getTakeAwayInfoById(id);
                result.setResultCode(ResultCode.COMMON_SUCCESS);
                result.setData(takeAway);
                break;
            case 2:
                TakeSend takeSend = takeSendService.getTakeSendInfoById(id);
                result.setResultCode(ResultCode.COMMON_SUCCESS);
                result.setData(takeSend);
                break;
        }
        return result;
    }


    /**
     * 删除订单（假删除）
     *
     * */
    @GetMapping("/deleteOrder")
    @ResponseBody
    public Result deleteOrder(
            @RequestParam(name = "id") String id,
            @RequestParam(name = "type") int type
    ){
        Result result = new Result();
        switch (type){
            case 1:
                takeAwayService.deleteTakeAwayById(id);
                result.setResultCode(ResultCode.COMMON_SUCCESS);
                break;
            case 2:
                takeSendService.deleteTakeSendById(id);
                result.setResultCode(ResultCode.COMMON_SUCCESS);
                break;
        }
        return result;
    }


    /**
     * 取消订单
     *
     * */
    @GetMapping("/cancelOrder")
    @ResponseBody
    public Result cancelOrder(
            @RequestParam(name = "id") String id,
            @RequestParam(name = "type") int type
    ){
        Result result = new Result();
        switch (type){
            case 1:
                takeAwayService.updateTakeAwayById(id);
                result.setResultCode(ResultCode.COMMON_SUCCESS);
                break;
            case 2:
                takeSendService.updateTakeSendById(id);
                result.setResultCode(ResultCode.COMMON_SUCCESS);
                break;
        }
        return result;
    }

    /**
     * 获取用户历史地址
     *
     * */
    @GetMapping("/getHistoryAddress")
    @ResponseBody
    public Result getHistoryAddress(HttpServletRequest request
    ){
        Result result = new Result();
        WeChatFans weChatFans = (WeChatFans) request.getSession().getAttribute("loginUser");
        List<Address> data = userService.getHistoryAddress(weChatFans.getOpenId());
        result.setResultCode(ResultCode.COMMON_SUCCESS);
        result.setData(data);
        return result;
    }



}
