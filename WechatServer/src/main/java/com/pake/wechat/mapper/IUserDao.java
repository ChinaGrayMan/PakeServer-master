package com.pake.wechat.mapper;

import com.pake.wechat.model.Address;
import com.pake.wechat.model.Order;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface IUserDao {

    List<Order> getOrders(@Param("openId") String openId);

    List<Address> getHistoryAddress(@Param("openId") String openId);

    Address getNewAddress(@Param("openId") String openId);
}
