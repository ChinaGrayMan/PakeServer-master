package com.pake.wechat.mapper;

import com.pake.wechat.model.TakeSend;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface ITakeSendDao {

    void pleaseOrder(TakeSend takeSend);

    TakeSend getTakeSendInfoById(@Param("id") String id);

    void deleteTakeSendById(@Param("id") String id);

    void updateTakeSendById(@Param("id") String id);
}
