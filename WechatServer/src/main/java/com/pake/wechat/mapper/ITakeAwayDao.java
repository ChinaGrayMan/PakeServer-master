package com.pake.wechat.mapper;

import com.pake.wechat.model.TakeAway;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface ITakeAwayDao{

    void pleaseOrder(TakeAway takeAway);

    TakeAway getTakeAwayInfoById(@Param("id") String id);

    void deleteTakeAwayById(@Param("id") String id);

    void updateTakeAwayById(@Param("id") String id);
}
