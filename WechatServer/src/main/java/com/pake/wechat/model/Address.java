package com.pake.wechat.model;

import lombok.Data;

import java.io.Serializable;

@Data
public class Address implements Serializable {
    private static final long serialVersionUID = 5781151142608254700L;
    private  String name;
    private  String phone;
    private  String address;
}
