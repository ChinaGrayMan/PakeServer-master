package com.pake.wechat.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class TakeAway implements Serializable {
    //主键
    private String id;
    //openId
    private String openId;
    //短信消息
    private String sms;
    //备注
    private String note;
    //收件地址
    private String endUserNote;
    //收件人
    private String endUserName;
    //收件电话
    private String endPhone;
    //是否预约收件 0 否 1 是
    private int isSubscribe;
    //预约时间
    @JsonFormat(pattern="yyyy年MM月dd日 hh:mm:ss",timezone = "GMT+8")
    private Date subscribeTime;
    //状态 1待接单 2已接单 3已完成 4已取消 0已删除 5已拒接 6待补价钱
    private int state;
    //配送员ID
    private String disId;
    //配送员姓名
    private String disName;
    //配送员电话
    private String disPhone;
    //补差价原因
    private String bootMessage;
    //差价金额
    private int bootMoney;
    //拒接理由
    private String refuseMessage;
    //短信接收时间
    private String messageTime;
    //微信订单ID
    private String transactionId;
    //支付状态 0 未支付 1已支付
    private int payState;
    //创建时间
    @JsonFormat(pattern="yyyy年MM月dd日 hh:mm:ss",timezone = "GMT+8")
    private Date createDate;
}
