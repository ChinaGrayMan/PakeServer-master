package com.pake.wechat.model;


import lombok.Data;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;

@Data
public class WeChatFans implements Serializable{

    private String openId;
    /**unionid**/
    private String unionid;
    /**订阅状态**/
    private Integer subscribeStatus;
    /**订阅时间**/
    private String subscribeTime;
    /**昵称,二进制保存emoji表情**/
    private byte[] nickName;
    /**昵称显示**/
    private String nickNameStr;
    /**微信号**/
    private String weChatNumber;
    /**性别 0-女；1-男；2-未知**/
    private Integer gender;
    /**语言**/
    private String language;
    /**国家**/
    private String country;
    /**省**/
    private String province;
    /**城市**/
    private String city;//
    /**头像**/
    private String headimgurl;
    /**备注**/
    private String remark;//
    /**用户状态 1-可用；0-不可用**/
    private Integer status;

    public String getNicknameStr() {
        if (this.getNickName() != null) {
            try {
                this.nickNameStr = new String(this.getNickName(), "UTF-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
        return nickNameStr;
    }
}
