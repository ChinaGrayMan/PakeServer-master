package com.pake.wechat.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class Order implements Serializable{

    private static final long serialVersionUID = -7943375548973234985L;

    private String id;

    private int state;

    private int type;

    @JsonFormat(pattern="yyyy/MM/dd hh:mm:ss",timezone = "GMT+8")
    private Date createDate;


}
