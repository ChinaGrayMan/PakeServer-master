package com.pake.wechat.service.impl;


import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.pake.wechat.mapper.IUserDao;
import com.pake.wechat.model.Address;
import com.pake.wechat.model.Order;
import com.pake.wechat.service.IUserService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class UserServiceImpl implements IUserService {

    @Resource
    private IUserDao userDao;

    @Override
    public List<Order> getOrders(String openId, int pageNum, int pageSize) {
        Page<Order> page = PageHelper.startPage(pageNum, pageSize);
        userDao.getOrders(openId);
        PageInfo<Order> info = new PageInfo<>(page);
        return info.getList();
    }

    @Override
    public List<Address> getHistoryAddress(String openId) {
        return userDao.getHistoryAddress(openId);
    }

    @Override
    public Address getNewAddress(String openId) {
        return userDao.getNewAddress(openId);
    }
}
