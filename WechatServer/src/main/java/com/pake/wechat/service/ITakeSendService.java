package com.pake.wechat.service;


import com.pake.wechat.model.TakeSend;

public interface ITakeSendService {

    void pleaseOrder(TakeSend takeSend);

    TakeSend getTakeSendInfoById(String id);

    void deleteTakeSendById(String id);

    void updateTakeSendById(String id);
}
