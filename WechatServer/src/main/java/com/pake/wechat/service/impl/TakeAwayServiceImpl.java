package com.pake.wechat.service.impl;

import com.pake.wechat.mapper.ITakeAwayDao;
import com.pake.wechat.model.TakeAway;
import com.pake.wechat.service.ITakeAwayService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class TakeAwayServiceImpl implements ITakeAwayService {

    @Resource
    private ITakeAwayDao takeAwayDao;

    @Override
    public void pleaseOrder(TakeAway takeAway) {
        takeAwayDao.pleaseOrder(takeAway);
    }

    @Override
    public TakeAway getTakeAwayInfoById(String id) {
        return takeAwayDao.getTakeAwayInfoById(id);
    }

    @Override
    public void deleteTakeAwayById(String id) {
        takeAwayDao.deleteTakeAwayById(id);
    }

    @Override
    public void updateTakeAwayById(String id) {
        takeAwayDao.updateTakeAwayById(id);
    }
}
