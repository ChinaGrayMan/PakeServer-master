package com.pake.wechat.service;


import com.pake.wechat.model.TakeAway;

public interface ITakeAwayService {

    void pleaseOrder(TakeAway takeAway);

    TakeAway getTakeAwayInfoById(String id);

    void deleteTakeAwayById(String id);

    void updateTakeAwayById(String id);
}
