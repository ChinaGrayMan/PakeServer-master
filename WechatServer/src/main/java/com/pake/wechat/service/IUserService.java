package com.pake.wechat.service;

import com.pake.wechat.model.Address;
import com.pake.wechat.model.Order;

import java.util.List;

public interface IUserService {

    List<Order> getOrders(String openId, int pageNum, int pageSize);

    List<Address> getHistoryAddress(String openId);

    Address getNewAddress(String openId);
}
