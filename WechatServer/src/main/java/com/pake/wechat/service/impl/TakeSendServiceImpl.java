package com.pake.wechat.service.impl;

import com.pake.wechat.mapper.ITakeSendDao;
import com.pake.wechat.model.TakeSend;
import com.pake.wechat.service.ITakeSendService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class TakeSendServiceImpl implements ITakeSendService {

    @Resource
    private ITakeSendDao takeSendDao;

    @Override
    public void pleaseOrder(TakeSend takeSend) {
        takeSendDao.pleaseOrder(takeSend);
    }

    @Override
    public TakeSend getTakeSendInfoById(String id) {
        return takeSendDao.getTakeSendInfoById(id);
    }

    @Override
    public void deleteTakeSendById(String id) {
        takeSendDao.deleteTakeSendById(id);
    }

    @Override
    public void updateTakeSendById(String id) {
        takeSendDao.updateTakeSendById(id);
    }
}
