package com.pake.wechat.weChatPay.service.impl;

import com.lly835.bestpay.enums.BestPayTypeEnum;
import com.lly835.bestpay.model.PayRequest;
import com.lly835.bestpay.model.PayResponse;
import com.lly835.bestpay.model.RefundRequest;
import com.lly835.bestpay.model.RefundResponse;
import com.lly835.bestpay.service.BestPayService;
import com.lly835.bestpay.utils.JsonUtil;
import com.pake.wechat.weChatPay.service.IWechatPayService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
@Slf4j
public class WeChatpayServiceImpl implements IWechatPayService {

    @Resource
    private BestPayService bestPayService;

    @Override
    public PayResponse create(String orderId,String orderName,double orderAmount,String openId) {
        PayRequest payRequest = new PayRequest();
        payRequest.setPayTypeEnum(BestPayTypeEnum.WXPAY_H5);//支付方式，微信公众账号支付
        payRequest.setOrderId(orderId);//订单号.
        payRequest.setOrderName(orderName);//订单名字.
        payRequest.setOrderAmount(orderAmount);//订单金额.
        payRequest.setOpenid(openId);//微信openid, 仅微信支付时需要
        log.info("【微信支付】request={}", JsonUtil.toJson(payRequest));//将payRequest格式化一下，再显示在日志上，便于观看数据
        PayResponse payResponse = bestPayService.pay(payRequest);
        log.info("【微信支付】response={}", JsonUtil.toJson(payResponse));//将payResponse格式化一下，再显示在日志上，便于观看响应后返回的数据);
        return payResponse;
    }


    /**
     * 退款
     */
    @Override
    public RefundResponse refund(String orderId) {
        RefundRequest refundRequest = new RefundRequest();
        refundRequest.setOrderId("");
        refundRequest.setOrderAmount(12.0);
        refundRequest.setPayTypeEnum(BestPayTypeEnum.WXPAY_H5);
        log.info("【微信退款】request={}", JsonUtil.toJson(refundRequest));
        RefundResponse refundResponse = bestPayService.refund(refundRequest);
        log.info("【微信退款】response={}", JsonUtil.toJson(refundResponse));
        return refundResponse;
    }

    @Override
    public PayResponse asyncNotify(String notifyData) {
        return  bestPayService.asyncNotify(notifyData);
    }
}

