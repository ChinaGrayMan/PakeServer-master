package com.pake.wechat.weChatPay.ctrl;

import com.alibaba.fastjson.JSONObject;
import com.pake.wechat.common.result.Result;
import com.pake.wechat.common.result.ResultCode;
import com.pake.wechat.model.MpAccount;
import com.pake.wechat.util.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;


/**
 * 微信与开发者服务器交互接口
 */
@Controller
@RequestMapping("/weChatApi")
@Slf4j
public class WeChatApiCtrl {

    /**
     * 发送客服消息
     */
    @RequestMapping(value = "/sendMessageByOpenId", method = RequestMethod.POST)
    public void sendCustomTextMsg(HttpServletResponse response, String openid,String content) {
        MpAccount mpAccount = WxMemoryCacheClient.getSingleMpAccount();
        JSONObject result = WxApiClient.sendCustomTextMessage(openid, content, mpAccount);
        try {
            if (result.getInteger("errcode") != 0) {
                response.getWriter().write("send failure");
            } else {
                response.getWriter().write("send success");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 发送模板消息
     */
    @RequestMapping(value = "/sendTemplateMessage", method = RequestMethod.POST)
    public void sendTemplateMessage(HttpServletRequest request, HttpServletResponse response, String openid) {
        MpAccount mpAccount = WxMemoryCacheClient.getSingleMpAccount();//获取缓存中的唯一账号
        TemplateMessage tplMsg = new TemplateMessage();

        tplMsg.setOpenid(openid);
        //微信公众号号的template id，开发者自行处理参数
        tplMsg.setTemplateId("Wyme6_kKUqv4iq7P4d2NVldw3YxZIql4sL2q8CUES_Y");

        tplMsg.setUrl("http://www.weixinpy.com");
        Map<String, String> dataMap = new HashMap<String, String>();
        dataMap.put("first", "微信派官方微信模板消息测试");
        dataMap.put("keyword1", "时间：" + DateUtil.COMMON.getDateText(new Date()));
        dataMap.put("keyword2", "关键字二：你好");
        dataMap.put("remark", "备注：感谢您的来访");
        tplMsg.setDataMap(dataMap);

        JSONObject result = WxApiClient.sendTemplateMessage(tplMsg, mpAccount);
        try {
            if (result.getInteger("errcode") != 0) {
                response.getWriter().write("send failure");
            } else {
                response.getWriter().write("send success");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 获取js ticket
     */
    @PostMapping(value = "/jsTicket")
    @ResponseBody
    public Result jsTicket(String url) {
        Result result = new Result();
        MpAccount mpAccount = WxMemoryCacheClient.getSingleMpAccount();//获取缓存中的唯一账号
        String jsTicket = WxApiClient.getJSTicket(mpAccount);
        WxSign sign = new WxSign(mpAccount.getAppId(), jsTicket, url);
        log.info(sign.toString());
        result.setResultCode(ResultCode.COMMON_SUCCESS);
        result.setData(sign);
        return result;
    }

}




