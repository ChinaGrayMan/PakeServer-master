package com.pake.wechat.weChatPay.service;

import com.lly835.bestpay.model.PayResponse;
import com.lly835.bestpay.model.RefundResponse;

public interface IWechatPayService {

     /** 统一下单*/
     PayResponse create(String orderId,String orderName,double orderAmount,String openId) ;

     /** 退 款*/
     RefundResponse refund(String orderId);

     /** 异步通知*/
     PayResponse asyncNotify(String notifyData);
}
