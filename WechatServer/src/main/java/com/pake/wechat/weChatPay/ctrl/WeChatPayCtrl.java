package com.pake.wechat.weChatPay.ctrl;

import com.lly835.bestpay.model.PayResponse;
import com.lly835.bestpay.model.RefundResponse;
import com.lly835.bestpay.utils.JsonUtil;
import com.pake.wechat.model.TakeAway;
import com.pake.wechat.model.WeChatFans;
import com.pake.wechat.service.ITakeAwayService;
import com.pake.wechat.weChatPay.service.IWechatPayService;
import com.pake.wechat.weChatPay.util.PayCallback;
import com.pake.wechat.weChatPay.util.XmlUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.catalina.servlet4preview.http.HttpServletRequest;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;


@Controller
@RequestMapping("/weChatPay")
@Slf4j
public class WeChatPayCtrl {

    @Resource
    private XmlUtil xmlUtil;

    @Resource
    private IWechatPayService wechatPayService;

    @Resource
    private ITakeAwayService takeAwayService;

    /**
     * 统一下单支付
     */
    @RequestMapping("/create")
    @ResponseBody
    public PayResponse create(@Param("orderId") String orderId, @Param("type") int type, HttpServletRequest request){
        HttpSession session = request.getSession(true);
        PayResponse payResponse = null;
        WeChatFans weChatFans = (WeChatFans) session.getAttribute("loginUser");
        switch (type){
            /**寄件单*/
            case 1:
                TakeAway takeAway =takeAwayService.getTakeAwayInfoById(orderId);
                if(takeAway == null){
                    throw new RuntimeException("订单号不存在");
                }
                payResponse = wechatPayService.create(takeAway.getId(),"寄件单",5.0,weChatFans.getOpenId());
                break;
        }
        return payResponse;
    }

    /**
     * 微信异步通知
     * @param notifyData
     */
    @PostMapping("/notify")
    public String notify(@RequestBody String notifyData) {
        PayResponse payResponse =wechatPayService.asyncNotify(notifyData);
        log.info("【微信支付】异步通知, payResponse={}", JsonUtil.toJson(payResponse));
        //查询订单
//        OrderDTO orderDTO = orderService.findOne(payResponse.getOrderId());

        //判断订单是否存在
//        if (orderDTO == null) {
//            log.error("【微信支付】异步通知, 订单不存在, orderId={}", payResponse.getOrderId());
//            throw new SellException(ResultEnum.ORDER_NOT_EXIST);
//        }

        //判断金额是否一致(0.10   0.1)
//        if (!MathUtil.equals(payResponse.getOrderAmount(), orderDTO.getOrderAmount().doubleValue())) {
//            log.error("【微信支付】异步通知, 订单金额不一致, orderId={}, 微信通知金额={}, 系统金额={}",
//                    payResponse.getOrderId(),
//                    payResponse.getOrderAmount(),
//                    orderDTO.getOrderAmount());
//            throw new SellException(ResultEnum.WXPAY_NOTIFY_MONEY_VERIFY_ERROR);
//        }

        //修改订单的支付状态
//        orderService.paid(orderDTO);

        //返回给微信处理结果
        return getPayCallback();
    }

    /**
     * 退款
     */
    @RequestMapping("/refund")
    @ResponseBody
    public RefundResponse refund(@Param("orderId") String orderId){
        RefundResponse refundResponse = wechatPayService.refund(orderId);
        return refundResponse;
    }

    /**
     * 生成收到支付结果的确认信息
     */
    public String getPayCallback() {
        PayCallback callback = new PayCallback();
        xmlUtil.getXstreamInclueUnderline().alias("xml", callback.getClass());
        String xml = xmlUtil.getXstreamInclueUnderline().toXML(callback);
        return xml;
    }

}
