package com.pake.wechat.weChatPay.util;

import lombok.Data;

/**
 * 支付成功回复
 */
@Data
public class PayCallback {
    private String return_code;
    private String return_msg;

    public PayCallback() {
        this.return_code = "SUCCESS";
        this.return_msg = "OK";
    }
}

