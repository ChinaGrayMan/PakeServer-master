package com.pake.wechat.interceptor;

import com.pake.wechat.common.redis.RedisUtil;
import com.pake.wechat.model.WeChatFans;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * 微信客户端用户请求验证拦截器
 */
@Component
public class WeChatOAuth2Interceptor extends HandlerInterceptorAdapter {

	@Resource
	private RedisUtil redisUtil;

	/*
	 * 进入controller层之前拦截请求
	 */
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object obj) {
		HttpSession session = request.getSession(true);
		String sessionId = request.getSession().getId();
		WeChatFans weChatFans = new WeChatFans();
		String openid = (String) redisUtil.get(sessionId);
		if(StringUtils.isBlank(openid)){
			System.out.println("用户第一次登录.............................");
			weChatFans.setOpenId("123");
			weChatFans.setHeadimgurl("http://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83eoibAfzl40ByBO4IqZGZSsGpR5cjNa6xo6VjQhRAPQALa4LiaM1YDIbQsU40qbrPHAogFtXrjoGH9qA/132");
			weChatFans.setNickName(new String("seven eleven").getBytes());
			session.setAttribute("loginUser", weChatFans);
			redisUtil.set(sessionId,weChatFans.getOpenId(),1700);
		}else {
			System.out.println("用户已经登录.............................");
			return true;
		}
		return true;
//		HttpSession session = request.getSession(true);
//		String sessionId = request.getSession().getId();
//		String openid = (String) redisUtil.get(sessionId);
//		if(StringUtils.isBlank(openid)){
//			String code = request.getParameter("code");
//			if(StringUtils.isNotBlank(code)){
//				try {
//					MpAccount mpAccount = WxMemoryCacheClient.getSingleMpAccount();
//					WeChatFans weChatFans = WxApiClient.getFansInfoByOauth(openid, mpAccount,code);
//					if(!StringUtils.isBlank(weChatFans.getOpenId())){
//						redisUtil.set(sessionId,weChatFans.getOpenId(),1700);
//						session.setAttribute("loginUser",weChatFans);
//						return true;
//					}
//				} catch (Exception e) {
//					e.printStackTrace();
//				}
//			}else{
//				MpAccount mpAccount = WxMemoryCacheClient.getSingleMpAccount();
//				String redirectUrl = HttpUtil.getRequestFullUriNoContextPath(request);
//				String state = OAuth2RequestParamHelper.prepareState(request);
//				String url = WxApi.getOAuthCodeUrl(mpAccount.getAppId(), redirectUrl, OAuthScope.Userinfo.toString(), state);
//				HttpUtil.redirectHttpUrl(request, response, url);
//				return false;
//			}
//		}else{
//			return true;
//		}
//		HttpUtil.redirectUrl(request, response, "/error/error");
//		return false;
	}

	/*
	 * 视图渲染之后的操作
	 */
	@Override
	public void afterCompletion(HttpServletRequest arg0, HttpServletResponse arg1, Object arg2, Exception arg3)
			throws Exception {

	}

	/*
	 * 处理请求完成后视图渲染之前的处理操作
	 */
	@Override
	public void postHandle(HttpServletRequest arg0, HttpServletResponse arg1, Object arg2, ModelAndView arg3)
			throws Exception {

	}



}

