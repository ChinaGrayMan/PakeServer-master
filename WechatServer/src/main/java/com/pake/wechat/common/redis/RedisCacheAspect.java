package com.pake.wechat.common.redis;


import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;


@Aspect
@Component
@Slf4j
public class RedisCacheAspect {

    @Resource
    private RedisUtil redisUtil;

    @Around("execution(* com.pake..*.*(..)) && @annotation(redisCache)")
    public Object redisCache(ProceedingJoinPoint pjp,RedisCache redisCache) throws Throwable {
        //得到类名、方法名和参数
        String redisResult;
        String className = pjp.getTarget().getClass().getName();
        String methodName = pjp.getSignature().getName();
        Object[] args = pjp.getArgs();
        //根据类名，方法名和参数生成key
        String key = genKey(className,methodName,args);
        log.info("生成的key[{}]",key);
        //得到被代理的方法
        Signature signature = pjp.getSignature();
        if(!(signature instanceof MethodSignature)){
            throw  new IllegalArgumentException();
        }
        Object result;
        if(!redisUtil.hasKey(key)) {
            log.info("缓存未命中");
            result = pjp.proceed(args);
            redisResult = JSON.toJSONString(result);
            redisUtil.set(key,redisResult,redisCache.cacheTime());
        } else{
            //缓存命中
            log.info("缓存命中");
            redisResult = redisUtil.get(key).toString();
            result = JSON.parseObject(redisResult,redisCache.type());
        }
        return result;
    }

    private String genKey(String className, String methodName, Object[] args) {
        StringBuilder sb = new StringBuilder("SpringBoot:");
        sb.append(className);
        sb.append("_");
        sb.append(methodName);
        sb.append("_");
        for (Object object: args) {
            log.info("obj:"+object);
            if(object!=null) {
                sb.append(object+"");
                sb.append("_");
            }
        }
        return sb.toString();
    }

}
