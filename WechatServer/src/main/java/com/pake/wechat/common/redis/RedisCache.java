package com.pake.wechat.common.redis;

import java.lang.annotation.*;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD,ElementType.TYPE})
@Documented
public @interface RedisCache {

    /**
     * @Description: 数据返回类型
     * @Param:
     * @return:
     * @Author:
     * @Date: 2018/5/16
     */
    Class type();

    /**
     * @Description: 数据缓存时间单位s秒
     * @Param:  默认10分钟
     * @return:
     * @Author:
     * @Date: 2018/5/16
     */
    int cacheTime() default 600;

}
