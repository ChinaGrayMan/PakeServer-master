package com.pake.wechat.util;

import lombok.Data;

/**
 * OAuth token
 */
@Data
public class OAuthAccessToken extends AccessToken {
	private String oauthAccessToken;
	private String openid;
	private String scope;
}

