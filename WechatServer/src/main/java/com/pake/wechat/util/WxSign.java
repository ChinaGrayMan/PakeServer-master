package com.pake.wechat.util;

import lombok.Data;

import java.util.SortedMap;
import java.util.TreeMap;

@Data
public class WxSign {
    private String appId;
    private String timestamp;
    private String nonceStr;
    private String signature;

    public WxSign(String appId, String jsTicket, String url) {
        String timestamp = String.valueOf(System.currentTimeMillis() / 1000);
        String nonceStr = SecurityUtil.getRandomString(8);
        SortedMap<String, String> map = new TreeMap<String, String>();
        map.put("jsapi_ticket", jsTicket);
        map.put("noncestr", nonceStr);
        map.put("timestamp", timestamp);
        map.put("url", url);
        this.appId = appId;
        this.nonceStr = nonceStr;
        this.timestamp = timestamp;
        this.signature = SignUtil.signature(map);
    }

}
