package com.pake.wechat.util;

import lombok.Data;

import java.util.Date;

@Data
public class BaseEntity {

    private Long id;
    private Date createtime = new Date();//创建时间
}
