package com.pake.wechat.util;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class AppUtil {
	
    //通过uuid生成16位唯一订单号码
    public static String getOrderIdByUUId() {
        int machineId = 1;//最大支持1-9个集群机器部署
        int hashCodeV = UUID.randomUUID().toString().hashCode();
        if(hashCodeV < 0) {//有可能是负数
            hashCodeV = - hashCodeV;
        }
        // 0 代表前面补充0     
        // 4 代表长度为4     
        // d 代表参数为正数型
        return machineId + String.format("%015d", hashCodeV);
    }
	
	 /** 
	 * @Description 将字符串中的emoji表情转换成可以在utf-8字符集数据库中保存的格式（表情占4个字节，需要utf8mb4字符集） 
	 * @param str 
	 *            待转换字符串 
	 * @return 转换后字符串 
	 * @throws UnsupportedEncodingException
	 *             exception 
	 */  
	public static String emojiToUtf8(String str)
	        throws UnsupportedEncodingException {
	    String patternString = "([\\x{10000}-\\x{10ffff}\ud800-\udfff])";
	  
	    Pattern pattern = Pattern.compile(patternString);
	    Matcher matcher = pattern.matcher(str);
	    StringBuffer sb = new StringBuffer();
	    while(matcher.find()) {  
	        try {  
	            matcher.appendReplacement(  
	                    sb,  
	                    "[["  
	                            + URLEncoder.encode(matcher.group(1),
	                                    "UTF-8") + "]]");  
	        } catch(UnsupportedEncodingException e) {
	            throw e;  
	        }  
	    }  
	    matcher.appendTail(sb);  
	    return sb.toString();  
	}  
	  
	/** 
	 * @Description 还原utf8数据库中保存的含转换后emoji表情的字符串 
	 * @param str 
	 *            转换后的字符串 
	 * @return 转换前的字符串 
	 * @throws UnsupportedEncodingException
	 *             exception 
	 */  
	public static String Utf8Toemoji(String str)
	        throws UnsupportedEncodingException {
	    String patternString = "\\[\\[(.*?)\\]\\]";
	  
	    Pattern pattern = Pattern.compile(patternString);
	    Matcher matcher = pattern.matcher(str);
	  
	    StringBuffer sb = new StringBuffer();
	    while(matcher.find()) {  
	        try {  
	            matcher.appendReplacement(sb,  
	                    URLDecoder.decode(matcher.group(1), "UTF-8"));
	        } catch(UnsupportedEncodingException e) {
	            throw e;  
	        }  
	    }  
	    matcher.appendTail(sb);  
	    return sb.toString();  
	}  

}
