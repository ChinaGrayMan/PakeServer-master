package com.pake.wechat.config;

import com.pake.wechat.model.MpAccount;
import com.pake.wechat.util.WxMemoryCacheClient;
import lombok.Data;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Data
@Component
@Order(value=1)
@ConfigurationProperties(prefix = "we-chat")
public class InitData implements CommandLineRunner {
    /**
     * 公众平台ID
     */
    private String mpAppId;
    /**
     * 公众平台密钥
     */
    private String mpAppSecret;

    @Override
    public void run(String... args){
        MpAccount mpAccount = new MpAccount(mpAppId,mpAppId,mpAppSecret);
        WxMemoryCacheClient.addMpAccount(mpAccount);
    }

}
