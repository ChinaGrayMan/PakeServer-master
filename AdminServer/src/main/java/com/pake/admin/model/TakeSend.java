package com.pake.admin.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class TakeSend implements Serializable {
    private static final long serialVersionUID = -3810204754361615987L;
    //主键
    private String id;
    //openId
    private String openId;
    //商品类型
    private String goodsType;
    //备注
    private String note;
    //取件地址
    private String endUserNote;
    //取件人
    private String endUserName;
    //取件电话
    private String endPhone;
    //是否预约收件 0 否 1 是
    private int isSubscribe;
    //预约时间
    @JsonFormat(pattern="yyyy年MM月dd日 hh:mm:ss",timezone = "GMT+8")
    private Date subscribeTime;
    //状态 1待接单 2已接单 3已完成 4已取消 0已删除
    private int state;
    //快递单号
    private String courierNumber;
    //快递公司
    private String courierCompany;
    //创建时间
    @JsonFormat(pattern="yyyy年MM月dd日 hh:mm:ss",timezone = "GMT+8")
    private Date createDate;
}
