package com.pake.admin.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class Distributor implements Serializable {
    private static final long serialVersionUID = -5516308416637403365L;
    private String id;
    //头像
    private String headImg;
    //工号
    private String idNum;
    //姓名
    private String name;
    //性别
    private String sex;
    //手机号
    private String phone;
    //身份证号码
    private String idCard;
    // 1:启用 2:停用
    private int state;
    //创建日期
    @JsonFormat(pattern="yyyy年MM月dd日 hh:mm:ss",timezone = "GMT+8")
    private Date createDate;
}
