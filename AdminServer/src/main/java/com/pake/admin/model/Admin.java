package com.pake.admin.model;


import lombok.Data;

@Data
public class Admin {
    private String id;
    private String userName;
    private String passWord;
    private String openId;
    private String nikeName;
}
