package com.pake.admin.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.pake.admin.mapper.ITakeAwayDao;
import com.pake.admin.model.TakeAway;
import com.pake.admin.service.ITakeAwayService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class TakeAwayServiceImpl implements ITakeAwayService {

    @Resource
    private ITakeAwayDao takeAwayDao;

    @Override
    public List<TakeAway> findAll(TakeAway takeAway, int pageNum, int pageSize) {
        Page<TakeAway> page = PageHelper.startPage(pageNum, pageSize);
        takeAwayDao.findAll(takeAway);
        PageInfo<TakeAway> info = new PageInfo<>(page);
        return info.getList();
    }

    @Override
    public void update(TakeAway takeAway) {
        takeAwayDao.update(takeAway);
    }

    @Override
    public void delete(String id) {
        takeAwayDao.delete(id);
    }

    @Override
    public TakeAway getTakeAwayById(String id) {
        return takeAwayDao.getTakeAwayById(id);
    }

    @Override
    public void sendOut(String id,String disId,String disName, String disPhone) {
        takeAwayDao.sendOut(id,disId,disName,disPhone);
    }

    @Override
    public void sendDone(String id) {
        takeAwayDao.sendDone(id);
    }

    @Override
    public void refuse(String id, String refuseMessage) {
        takeAwayDao.refuse(id,refuseMessage);
    }

    @Override
    public void boot(String id, int bootMoney,String bootMessage) {
        takeAwayDao.boot(id,bootMoney,bootMessage);
    }
}
