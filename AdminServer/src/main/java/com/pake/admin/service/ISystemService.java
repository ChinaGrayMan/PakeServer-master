package com.pake.admin.service;

import com.pake.admin.common.springSecurity.pojo.SysResource;
import com.pake.admin.common.springSecurity.pojo.SysRole;
import com.pake.admin.common.springSecurity.pojo.SysUser;

import javax.management.relation.Role;
import java.util.List;

public interface ISystemService {

    void roleBySave(SysRole sysRole);

    List<SysRole> getRoles();

    List<SysUser> getUsers();

    void userBySave(SysUser sysUser);

    List<SysResource> getMenus();
}
