package com.pake.admin.service.impl;

import com.pake.admin.mapper.IAdminDao;
import com.pake.admin.model.Admin;
import com.pake.admin.service.IAdminService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

@Service
public class AdminServiceImpl implements IAdminService {

    @Resource
    private IAdminDao adminDao;

    @Override
    public boolean login(Admin admin) {
        Admin adminTem = adminDao.getAdmin(admin);
        if (adminTem != null && !"".equals(adminTem)) {
            return true;
        }
        return false;
    }

    @Override
    public List<Map<String,Integer>> one() {
        return adminDao.one();
    }

    @Override
    public List<Map<String, String>> two() {
        return adminDao.two();
    }

    @Override
    public List<Map<String, Integer>> index() {
        return adminDao.index();
    }

    @Override
    public List<Map<String, String>> three() {
        return adminDao.three();
    }

    @Override
    public List<Map<String, String>> four() {
        return adminDao.four();
    }

}
