package com.pake.admin.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.pake.admin.mapper.ITakeSendDao;
import com.pake.admin.model.TakeSend;
import com.pake.admin.service.ITakeSendService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class TakeSendServiceImpl implements ITakeSendService {

    @Resource
    private ITakeSendDao takeSendDao;


    @Override
    public PageInfo findAll(TakeSend takeSend, int pageNum, int pageSize) {
        Page<TakeSend> page = PageHelper.startPage(pageNum, pageSize);
        takeSendDao.findAll(takeSend);
        PageInfo<TakeSend> info = new PageInfo<>(page);
        return info;
    }

    @Override
    public TakeSend getOne(TakeSend takeSend) {
        return takeSendDao.getOne(takeSend);
    }

    @Override
    public void update(TakeSend takeSend) {

    }

}
