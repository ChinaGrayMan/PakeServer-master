package com.pake.admin.service;

import com.github.pagehelper.PageInfo;
import com.pake.admin.model.TakeSend;

public interface ITakeSendService {

    PageInfo findAll(TakeSend take, int pageNum, int pageSize);

    TakeSend getOne(TakeSend takeSend);

    void update(TakeSend takeSend);
}
