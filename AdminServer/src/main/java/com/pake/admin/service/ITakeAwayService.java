package com.pake.admin.service;


import com.pake.admin.model.TakeAway;

import java.util.List;

public interface ITakeAwayService {

    List<TakeAway> findAll(TakeAway takeAway, int pageNum, int pageSize);

    void update(TakeAway takeAway);

    void delete(String id);

    TakeAway getTakeAwayById(String id);

    void sendOut(String id,String disId,String disName,String disPhone);

    void sendDone(String id);

    void refuse(String id, String refuseMessage);

    void boot(String id, int bootMoney,String bootMessage);
}
