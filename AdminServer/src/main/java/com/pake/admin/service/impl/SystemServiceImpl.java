package com.pake.admin.service.impl;

import com.pake.admin.common.springSecurity.pojo.SysResource;
import com.pake.admin.common.springSecurity.pojo.SysRole;
import com.pake.admin.common.springSecurity.pojo.SysUser;
import com.pake.admin.mapper.ISystemDao;
import com.pake.admin.mapper.ITakeSendDao;
import com.pake.admin.service.ISystemService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.management.relation.Role;
import java.util.List;

@Service
public class SystemServiceImpl implements ISystemService {

    @Resource
    private ISystemDao systemDao;

    @Override
    public void roleBySave(SysRole sysRole) {
        systemDao.roleBySave(sysRole);
    }

    @Override
    public List<SysRole> getRoles() {
        return systemDao.getRoles();
    }

    @Override
    public List<SysUser> getUsers() {
        return systemDao.getUsers();
    }

    @Override
    public void userBySave(SysUser sysUser) {
        systemDao.userBySave(sysUser);
    }

    @Override
    public List<SysResource> getMenus() {
        return systemDao.getMenus();
    }
}
