package com.pake.admin.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.pake.admin.mapper.IDistributorDao;
import com.pake.admin.model.Distributor;
import com.pake.admin.service.IDistributorService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class DistributorServiceImpl implements IDistributorService {

    @Resource
    private IDistributorDao distributorDao;

    @Override
    public List<Distributor> getAll(Distributor distributor) {
        return distributorDao.getAll(distributor);
    }

    @Override
    public void save(Distributor distributor) {
        distributorDao.save(distributor);
    }

    @Override
    public void stopDis(String id) {
        distributorDao.stopDis(id);
    }

    @Override
    public void runDis(String id) {
        distributorDao.runDis(id);
    }
}
