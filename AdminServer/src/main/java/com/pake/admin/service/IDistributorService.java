package com.pake.admin.service;

import com.pake.admin.model.Distributor;

import java.util.List;

public interface IDistributorService {

    List<Distributor> getAll(Distributor distributor);

    void save(Distributor distributor);

    void stopDis(String id);

    void runDis(String id);
}
