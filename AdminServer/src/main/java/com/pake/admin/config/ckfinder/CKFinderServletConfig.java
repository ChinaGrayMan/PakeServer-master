package com.pake.admin.config.ckfinder;

import com.ckfinder.connector.ConnectorServlet;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


/**
 * CkFinder 配置
 * @author WangJie
 *
 * */
@Configuration
public class CKFinderServletConfig {

    @Bean
    public ServletRegistrationBean connectCKFinder(){
        ServletRegistrationBean registrationBean=new ServletRegistrationBean(new ConnectorServlet(),"/static/ckfinder/core/connector/java/connector.java");
        registrationBean.addInitParameter("XMLConfig","classpath:/static/ckfinder/ckfinder.xml");
        registrationBean.addInitParameter("debug","false");
        registrationBean.addInitParameter("configuration","com.pake.admin.config.ckfinder.CKFinderConfig");
        return registrationBean;
    }

}
