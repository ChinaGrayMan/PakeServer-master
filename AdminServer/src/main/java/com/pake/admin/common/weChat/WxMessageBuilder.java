package com.pake.admin.common.weChat;


import com.alibaba.fastjson.JSONObject;

import java.util.Date;

/**
 * 消息builder工具类
 */
public class WxMessageBuilder {

    //客服文本消息
    public static String prepareCustomText(String openid, String content) {
        JSONObject jsObj = new JSONObject();
        jsObj.put("touser", openid);
        jsObj.put("msgtype", MsgType.Text.name());
        JSONObject textObj = new JSONObject();
        textObj.put("content", content);
        jsObj.put("text", textObj);
        return jsObj.toString();
    }

    //获取 MsgResponseText 对象
    public static MsgResponseText getMsgResponseText(MsgRequest msgRequest, MsgText msgText) {
        if (msgText != null) {
            MsgResponseText reponseText = new MsgResponseText();
            reponseText.setToUserName(msgRequest.getFromUserName());
            reponseText.setFromUserName(msgRequest.getToUserName());
            reponseText.setMsgType(MsgType.Text.toString());
            reponseText.setCreateTime(new Date().getTime());
            reponseText.setContent(msgText.getContent());
            return reponseText;
        } else {
            return null;
        }
    }
}
