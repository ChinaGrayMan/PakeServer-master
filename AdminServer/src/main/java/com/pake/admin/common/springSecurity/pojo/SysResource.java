package com.pake.admin.common.springSecurity.pojo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

/**
 * 资源的实体类
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class SysResource implements Serializable {
	private static final long serialVersionUID = -8823061289588424638L;
	/*ID*/
	private Long id;
	/*资源名称*/
	private String name;
	/*资源图标*/
	private String icon;
	/*资源链接*/
	private String url;
	/*父级ID*/
	private int pid;
	/*排序*/
	private int sort;
	/*创建日期*/
	@JsonFormat(pattern="yyyy年MM月dd日 hh:mm:ss",timezone = "GMT+8")
	private Date createDate;
}
