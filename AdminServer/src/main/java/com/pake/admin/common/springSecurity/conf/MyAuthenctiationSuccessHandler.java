package com.pake.admin.common.springSecurity.conf;

import com.pake.admin.common.springSecurity.pojo.SysUser;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component()
public class MyAuthenctiationSuccessHandler extends SimpleUrlAuthenticationSuccessHandler {

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
                                        Authentication authentication) throws IOException, ServletException {
        logger.info("登录成功");
        SysUser userDetails = (SysUser)authentication.getPrincipal();
        logger.info("管理员 " + userDetails.getUsername() + " 登录");
        response.sendRedirect("/admin/index");
    }

}
