package com.pake.admin.common.springSecurity.conf;

import java.util.*;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import com.pake.admin.common.springSecurity.dao.ResourceDao;
import com.pake.admin.common.springSecurity.dao.RoleResourceDao;
import com.pake.admin.common.springSecurity.pojo.SysResource;
import com.pake.admin.common.springSecurity.pojo.SysRole;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.ConfigAttribute;
import org.springframework.security.access.SecurityConfig;
import org.springframework.security.web.FilterInvocation;
import org.springframework.security.web.access.intercept.FilterInvocationSecurityMetadataSource;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.security.web.util.matcher.RequestMatcher;
import org.springframework.stereotype.Service;


/**
 * 系统启动时将资源和权限的对应信息关联起来
 * @author virtualspider
 *
 */
@Service
public class CustomInvocationSecurityMetadataSourceService implements FilterInvocationSecurityMetadataSource {

	@Resource
	private ResourceDao resourceDao;
	
	@Resource
	private RoleResourceDao roleResourceDao;
	
	private static Map<String, Collection<ConfigAttribute>> resourceMap = null;
	
	/**
	 * 在Web服务器启动时，提取系统中的所有权限。
	 * 先找出所有的资源url列表
	 * 然后遍历每个资源url，循环执行如下操作
	 * 		根据匹配关系找到对应的角色列表
	 * 		将匹配的角色列表封装，存入map
	 */
	@PostConstruct
	private void loadResourceDefine() {
		resourceMap = new HashMap<>();
		List<SysResource> resourceList = resourceDao.findAll();
		for(SysResource resource : resourceList) {
			List<SysRole> roles = roleResourceDao.findRolesByResourceUrl(resource.getId());
			String url = resource.getUrl();
			Collection<ConfigAttribute> atts = new ArrayList<>();
			for(SysRole role:roles) {
				ConfigAttribute ca = new SecurityConfig(role.getName());
				atts.add(ca);
			}
			resourceMap.put(url, atts);
		}
		System.out.println(resourceMap.toString());
	}
		
	/**
	 * 根据URL，找到相关的权限配置
	 */
	@Override
	public Collection<ConfigAttribute> getAttributes(Object object) throws IllegalArgumentException {
		System.out.println("nwuidhwuiehdfu");
		// object 是一个URL，被用户请求的url。
		FilterInvocation filterInvocation = (FilterInvocation) object;
		if (resourceMap == null) {
			loadResourceDefine();
		}
		Iterator<String> ite = resourceMap.keySet().iterator();
		while (ite.hasNext()) {
			String resURL = ite.next();
			 RequestMatcher requestMatcher = new AntPathRequestMatcher(resURL);
			    if(requestMatcher.matches(filterInvocation.getHttpRequest())) {
				return resourceMap.get(resURL);
			}
		}
 
		return null;
	}

	@Override
	public Collection<ConfigAttribute> getAllConfigAttributes() {
		return new ArrayList<>();
	}

	@Override
	public boolean supports(Class<?> clazz) {
		return true;
	}

}
