package com.pake.admin.common;

import org.springframework.stereotype.Component;

import java.io.Serializable;

/**
 * 返回结果
 * @author WangJie
 */
@Component
public class Result implements Serializable {

    private static final long serialVersionUID = 3824678594790776377L;

    /**
     * 结果码
     */
    private int code;

    /**
     * 描述
     */
    private String msg;

    /**
     * 自定义属性
     */
    private Object data;

    public Object getData() {
        return data;
    }

    public Result(ResultCode resultCode) {
        this.code = resultCode.getCode();
        this.msg = resultCode.getDescription();
    }

    public void setData(Object data) {
        this.data = data;
    }

    public Result() {

    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public void setResultCode(ResultCode resultCode) {
        setCode(resultCode.getCode());
        setMsg(resultCode.getDescription());
    }

    @Override
    public String toString() {
        return "Result [code=" + code + ", msg=" + msg + ", data=" + data + "]";
    }

}
