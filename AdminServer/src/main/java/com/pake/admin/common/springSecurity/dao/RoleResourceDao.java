package com.pake.admin.common.springSecurity.dao;


import com.pake.admin.common.springSecurity.pojo.SysResource;
import com.pake.admin.common.springSecurity.pojo.SysRole;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;


/**
 * 角色资源中间表的dao类
 * @author virtualspider
 *
 */
@Mapper
public interface RoleResourceDao {
	/**
	 * 通过资源的id获取所有的角色
	 * 		先通过role_resource中间表的resource_id查出对应的role_id
	 * 		再通过role表的id查出对应的所有信息
	 * @return
	 */
	@Select("SELECT r.* \r\n" + 
			"FROM security_role_resource rr \r\n" +
			"LEFT JOIN security_role r ON rr.role_id=r.id\n" +
			"WHERE rr.resource_id=#{resourceId}")
	List<SysRole> findRolesByResourceUrl(Long resourceId);

	@Select("SELECT DISTINCT\n" +
			"\tb.*, a.resource_id\n" +
			"FROM\n" +
			"\t`security_role_resource` AS a\n" +
			"LEFT JOIN security_resource AS b ON a.resource_id = b.id\n" +
			"WHERE\n" +
			"\trole_id IN (#{rolesId})\n" +
			"ORDER BY\n" +
			"\tsort;\n")
	List<SysResource> getResourcesByRole(String rolesId);
}
