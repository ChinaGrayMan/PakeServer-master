package com.pake.admin.common;

/**
 * 结果码枚举
 * @author WangJie
 */
public enum ResultCode {
    /**
     * 通用的返回码
     */
    COMMON_SUCCESS(0, "成功"),
    COMMON_ERROR(1, "失败");
    /**
     * 结果码
     */
    private int code;
    /**
     * 描述
     */
    private String description;

    ResultCode(int code, String description) {
        this.code = code;
        this.description = description;
    }

    public int getCode() {
        return code;
    }

    public String getDescription() {
        return description;
    }

}
