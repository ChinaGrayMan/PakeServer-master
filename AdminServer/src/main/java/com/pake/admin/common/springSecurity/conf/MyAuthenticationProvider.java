package com.pake.admin.common.springSecurity.conf;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.pake.admin.common.springSecurity.pojo.MyUserDetails;
import com.pake.admin.common.springSecurity.service.CustomUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Component;


/**
 * 自定义用户名和密码验证
 * @author Administrator
 *
 */
@Component
public class MyAuthenticationProvider implements AuthenticationProvider {

	@Autowired
	private CustomUserDetailsService myUserDetailService;
	
	@Override
	public Authentication authenticate(Authentication authentication) throws AuthenticationException {
		String username = authentication.getName();
		String password = (String) authentication.getCredentials();
		MyUserDetails user = (MyUserDetails) myUserDetailService.loadUserByUsername(username);
		if(user==null) {
			throw new BadCredentialsException("用户名未找到");
		}
		//加密过程在这里体现
		if(!password.equals(user.getPassword())) {
			throw new BadCredentialsException("密码不正确");
		}
		Collection<? extends GrantedAuthority> authorities = user.getAuthorities();
		return new UsernamePasswordAuthenticationToken(user, password, authorities);
	}


	@Override
	public boolean supports(Class<?> authentication) {
		return authentication.equals(UsernamePasswordAuthenticationToken.class);
	}

}
