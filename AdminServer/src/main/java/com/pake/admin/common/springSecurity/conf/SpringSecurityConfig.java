package com.pake.admin.common.springSecurity.conf;

import com.pake.admin.common.springSecurity.service.CustomUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.security.core.session.SessionRegistryImpl;
import org.springframework.security.web.authentication.RememberMeServices;
import org.springframework.security.web.authentication.rememberme.JdbcTokenRepositoryImpl;
import org.springframework.security.web.authentication.rememberme.PersistentTokenBasedRememberMeServices;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import javax.sql.DataSource;

/**
 * springsecurity的自定义配置类
 * @author Administrator
 *
 */
@Configuration
public class SpringSecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
    private MyAuthenticationProvider provider;

	//注入数据源

	@Autowired
	@Qualifier("dataSource")
	private DataSource dataSource;

	@Autowired
	private CustomUserDetailsService customUserDetailsService;

	/**
	 * 静态文件，忽略拦截
	 */
	@Override
	public void configure(WebSecurity web){
		web.ignoring().antMatchers("/static/**");
		web.ignoring().antMatchers("/public/images/**");
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {

			http.authorizeRequests()
				.antMatchers( "/")
				.permitAll()
				.anyRequest().authenticated();

			http.rememberMe()
				.rememberMeServices(rememberMeServices())
				.tokenValiditySeconds(60 * 60 * 24 * 7)
				.key("INTERNAL_SECRET_KEY");

			http.formLogin()
				.loginPage("/login")
				.permitAll()
				.successHandler(loginSuccessHandler())
				.failureHandler(loginFailHandler());

			http.logout()
					.logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
					.logoutSuccessUrl("/login")
					.invalidateHttpSession(true)
					.permitAll();

			http.sessionManagement()
				.maximumSessions(1)
				.sessionRegistry(sessionRegistry())
				.expiredUrl("/login");

			http.headers().frameOptions().disable();

			http.csrf().disable();

	}

	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		//自定义验证
		auth.authenticationProvider(provider);
		//不删除凭据，以便记住用户
		auth.eraseCredentials(false);
	}

	/**
	 * 返回 RememberMeServices 实例
	 */
	@Bean
	public RememberMeServices rememberMeServices() {
		JdbcTokenRepositoryImpl rememberMeTokenRepository = new JdbcTokenRepositoryImpl();
		rememberMeTokenRepository.setDataSource(dataSource);
		PersistentTokenBasedRememberMeServices rememberMeServices =
				new PersistentTokenBasedRememberMeServices("INTERNAL_SECRET_KEY", customUserDetailsService, rememberMeTokenRepository);
		rememberMeServices.setParameter("remember-me");
		return rememberMeServices;
	}

	/*
	*loginSuccessHandler
	*
	**/
	@Bean
	public MyAuthenctiationSuccessHandler loginSuccessHandler(){
		return new MyAuthenctiationSuccessHandler();
	}

	/*
	 *loginFailHandler
	 *
	 **/
	@Bean
	public MyAuthenctiationFailureHandler loginFailHandler(){
		return new MyAuthenctiationFailureHandler();
	}


	/*
	 *Session 并发控制
	 *
	 **/
	@Bean
	public SessionRegistry sessionRegistry(){
		return new SessionRegistryImpl();
	}

}
