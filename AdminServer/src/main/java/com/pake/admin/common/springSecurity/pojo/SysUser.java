package com.pake.admin.common.springSecurity.pojo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

/**
 * 用户
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class SysUser implements Serializable{
	private static final long serialVersionUID = -8716738875163099744L;
	/*ID*/
	private Long id;
	/*用户名*/
	private String username;
	/*密码*/
	private String password;
	/*头像*/
	private String headerImg;
	/*创建日期*/
	@JsonFormat(pattern="yyyy年MM月dd日 hh:mm:ss",timezone = "GMT+8")
	private Date createDate;

	public SysUser(SysUser user) {
		this.id = user.getId();
		this.username = user.getUsername();
		this.password = user.getPassword();
	}


}
