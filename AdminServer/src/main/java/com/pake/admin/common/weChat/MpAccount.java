package com.pake.admin.common.weChat;

import lombok.Data;

import java.io.Serializable;

/**
 * 微信公众号信息
 */
@Data
public class MpAccount implements Serializable {
	private static final long serialVersionUID = -6315146640254918207L;
	private String account;
	private String appId;
	private String appSecret;

	public MpAccount(String account, String appId, String appSecret) {
		this.account = account;
		this.appId = appId;
		this.appSecret = appSecret;
	}
}
