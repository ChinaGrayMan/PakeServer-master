package com.pake.admin.common.weChatPay.service;

import com.lly835.bestpay.model.RefundResponse;

public interface IWechatPayService {

     /** 退 款*/
     RefundResponse refund(String orderId);

}
