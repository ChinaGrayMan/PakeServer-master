package com.pake.admin.common.weChat;


import lombok.Data;

import java.io.Serializable;
import java.util.Map;

/**
 * 发送的模板消息对象
 */
@Data
public class TemplateMessage implements Serializable{

    private static final long serialVersionUID = 8164265894276551061L;

    private String openid;//粉丝id
    private String templateId;//模板id
    private String url;//链接
    private String color = "#000";//颜色
    private Map<String, String> dataMap;//参数数据

}
