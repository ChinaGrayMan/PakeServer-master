package com.pake.admin.common.springSecurity.pojo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

/**
 * 角色
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class SysRole implements Serializable{
	private static final long serialVersionUID = -6916352978213813430L;
	/*ID*/
	private Long id;
	/*角色名*/
	private String name;
	/*备注*/
	private String remarks;
	/*创建日期*/
	@JsonFormat(pattern="yyyy年MM月dd日 hh:mm:ss",timezone = "GMT+8")
	private Date createDate;
}
