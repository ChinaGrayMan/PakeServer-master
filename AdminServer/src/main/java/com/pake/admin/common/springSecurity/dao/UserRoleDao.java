package com.pake.admin.common.springSecurity.dao;


import com.pake.admin.common.springSecurity.pojo.SysRole;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface UserRoleDao {

	/**
	 * 通过用户id获取角色信息
	 * @param userId
	 * @return
	 */
	@Select("SELECT r.id,r.name FROM security_user_role ur\r\n" +
			"LEFT JOIN security_role r ON ur.`role_id`=r.`id`\r\n" +
			"WHERE ur.id=#{userId}")
	List<SysRole> getRolesByUserId(Long userId);
	
}
