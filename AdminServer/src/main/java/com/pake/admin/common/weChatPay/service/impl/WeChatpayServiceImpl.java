package com.pake.admin.common.weChatPay.service.impl;

import com.lly835.bestpay.enums.BestPayTypeEnum;
import com.lly835.bestpay.model.RefundRequest;
import com.lly835.bestpay.model.RefundResponse;
import com.lly835.bestpay.service.BestPayService;
import com.lly835.bestpay.utils.JsonUtil;
import com.pake.admin.common.weChatPay.service.IWechatPayService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
@Slf4j
public class WeChatpayServiceImpl implements IWechatPayService {

    @Resource
    private BestPayService bestPayService;

    /**
     * 退款
     */
    @Override
    public RefundResponse refund(String orderId) {
        RefundRequest refundRequest = new RefundRequest();
        refundRequest.setOrderId("");
        refundRequest.setOrderAmount(12.0);
        refundRequest.setPayTypeEnum(BestPayTypeEnum.WXPAY_H5);
        log.info("【微信退款】request={}", JsonUtil.toJson(refundRequest));
        RefundResponse refundResponse = bestPayService.refund(refundRequest);
        log.info("【微信退款】response={}", JsonUtil.toJson(refundResponse));
        return refundResponse;
    }

}

