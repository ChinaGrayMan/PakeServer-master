package com.pake.admin.common.springSecurity.service;

import com.pake.admin.common.springSecurity.dao.RoleResourceDao;
import com.pake.admin.common.springSecurity.dao.UserDao;
import com.pake.admin.common.springSecurity.dao.UserRoleDao;
import com.pake.admin.common.springSecurity.pojo.MyUserDetails;
import com.pake.admin.common.springSecurity.pojo.SysResource;
import com.pake.admin.common.springSecurity.pojo.SysRole;
import com.pake.admin.common.springSecurity.pojo.SysUser;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

@Component
@Slf4j
public class CustomUserDetailsService implements UserDetailsService {

	@Autowired
	private HttpSession session;

	@Resource
	private UserDao userDao;

	@Resource
	private UserRoleDao userRoleDao;

	@Resource
	private RoleResourceDao roleResourceDao;

	@Override
	public UserDetails loadUserByUsername (String username) throws UsernameNotFoundException {
			SysUser user = userDao.findUserByUsername(username);
			if (user == null) {
				log.error("user {} login failed, username or password is wrong", username);
				throw new BadCredentialsException("Username or password is not correct");
			}
			List<SysRole> roles = userRoleDao.getRolesByUserId(user.getId());
			StringBuilder commaBuilder = new StringBuilder();
			for(SysRole role : roles){
				commaBuilder.append(role.getId()).append(",");
				log.debug("role {} by {}", role,username);
			}
			String rolesId = commaBuilder.substring(0,commaBuilder.length()-1);
			validateUser(username, user,rolesId);
			return new MyUserDetails(user,roles);
	}

	private void validateUser(String username, SysUser user,String rolesId) {
		log.debug("user {} login success", username);
		List<SysResource> sysResources = roleResourceDao.getResourcesByRole(rolesId);
		session.setAttribute("menus", sysResources);
		session.setAttribute("loginUser", user);
	}

}
