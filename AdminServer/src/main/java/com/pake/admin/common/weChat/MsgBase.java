package com.pake.admin.common.weChat;

import lombok.Data;

/**
 * 消息基本信息
 */
@Data
public class MsgBase extends BaseEntity {

    private String msgtype;//消息类型;
    private String inputcode;//关注者发送的消息
    private String rule;//规则，目前是 “相等”
    private Integer enable;//是否可用
    private Integer readcount;//消息阅读数
    private Integer favourcount;//消息点赞数

}