package com.pake.admin.common.springSecurity.dao;

import com.pake.admin.common.springSecurity.pojo.SysUser;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface UserDao {

	@Select("select * from security_user where username=#{username}")
	SysUser findUserByUsername(String username);

	@Select("select * from security_user")
	List<SysUser> getAll();
}
