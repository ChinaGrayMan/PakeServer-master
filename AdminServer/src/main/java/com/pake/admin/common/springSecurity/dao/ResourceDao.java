package com.pake.admin.common.springSecurity.dao;

import java.util.List;

import com.pake.admin.common.springSecurity.pojo.SysResource;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

/**
 * resource资源表对应的dao类
 * @author virtualspider
 *
 */
@Mapper
public interface ResourceDao {
	@Select("select * from security_resource")
	List<SysResource> findAll();
}
