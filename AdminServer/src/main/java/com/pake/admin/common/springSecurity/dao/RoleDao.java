package com.pake.admin.common.springSecurity.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

/**
 * 角色表的中间类
 * @author virtualspider
 *
 */
@Mapper
public interface RoleDao {

	/**
	 * 查出所有的角色列表
	 */
	@Select("select * from security_role")
	 List<String> findAll();

}
