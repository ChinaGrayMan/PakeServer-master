package com.pake.admin.controller;

import com.pake.admin.common.Result;
import com.pake.admin.common.ResultCode;
import com.pake.admin.model.Distributor;
import com.pake.admin.model.TakeAway;
import com.pake.admin.service.IDistributorService;
import com.pake.admin.service.ITakeAwayService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/distributor")
public class DistributorCtrl {

    @Autowired
    private IDistributorService distributorService;

    @GetMapping("/list")
    public Result list(
            Distributor distributor) {
        Result result = new Result();
        List<Distributor> data =distributorService.getAll(distributor);
        result.setData(data);
        result.setResultCode(ResultCode.COMMON_SUCCESS);
        return result;
    }

    @PostMapping("/save")
    public Result save(
            Distributor distributor) {
        Result result = new Result();
        distributorService.save(distributor);
        result.setResultCode(ResultCode.COMMON_SUCCESS);
        return result;
    }

    @GetMapping("/stopDis")
    public Result stopDis(
            @RequestParam("id") String id) {
        Result result = new Result();
        distributorService.stopDis(id);
        result.setResultCode(ResultCode.COMMON_SUCCESS);
        return result;
    }

    @GetMapping("/runDis")
    public Result runDis(
            @RequestParam("id") String id) {
        Result result = new Result();
        distributorService.runDis(id);
        result.setResultCode(ResultCode.COMMON_SUCCESS);
        return result;
    }


}