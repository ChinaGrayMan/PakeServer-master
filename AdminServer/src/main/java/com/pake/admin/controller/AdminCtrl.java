package com.pake.admin.controller;

import com.pake.admin.model.Admin;
import com.pake.admin.service.IAdminService;
import com.pake.admin.service.ITakeAwayService;
import com.pake.admin.service.ITakeSendService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import javax.servlet.http.HttpSession;

/**
 * 系统管理后台核心控制器
 *
 * @author Jiew
 */
@Controller
@RequestMapping("/admin")
public class AdminCtrl {

    /**
     * 首页
     */
    @RequestMapping("/index")
    public ModelAndView index() {
        ModelAndView mv = new ModelAndView("admin/index");
        return mv;
    }

    /**
     * 取件订单
     */
    @RequestMapping("/takeAwayOrders")
    public String takeAwayOrders() {
        return "takeAway/list";
    }

    /**
     * 收件订单
     */
    @RequestMapping("/takeSendOrders")
    public String takeSendOrders() {
        return "takeSend/list";
    }

    /**
     * 文件管理
     */
    @RequestMapping("/files")
    public String files() {
        return "file/files";
    }

    /**
     * 配送员管理
     */
    @RequestMapping("/distributor")
    public String distributor() {
        return "distributor/list";
    }

    @RequestMapping("/user")
    public String user(){
        return "/system/user";
    }


    @RequestMapping("/role")
    public String role(){
        return "/system/role";
    }


    @RequestMapping("/meun")
    public String meun(){
        return "/system/meun";
    }

}



