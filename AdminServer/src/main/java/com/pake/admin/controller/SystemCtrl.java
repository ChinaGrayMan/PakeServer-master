package com.pake.admin.controller;

import com.pake.admin.common.Result;
import com.pake.admin.common.ResultCode;
import com.pake.admin.common.springSecurity.pojo.SysResource;
import com.pake.admin.common.springSecurity.pojo.SysRole;
import com.pake.admin.common.springSecurity.pojo.SysUser;
import com.pake.admin.service.ISystemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/system")
public class SystemCtrl {

    @Autowired
    private ISystemService systemService;

    @GetMapping("/getRoles")
    public Result getRoles() {
        Result result = new Result();
        List<SysRole> data = systemService.getRoles();
        result.setData(data);
        result.setResultCode(ResultCode.COMMON_SUCCESS);
        return result;
    }

    @PostMapping("/roleBySave")
    public Result roleBySave(SysRole sysRole) {
        Result result = new Result();
        systemService.roleBySave(sysRole);
        result.setData(sysRole);
        result.setResultCode(ResultCode.COMMON_SUCCESS);
        return result;
    }


    @GetMapping("/getUsers")
    public Result getUsers() {
        Result result = new Result();
        List<SysUser> data = systemService.getUsers();
        result.setData(data);
        result.setResultCode(ResultCode.COMMON_SUCCESS);
        return result;
    }

    @PostMapping("/userBySave")
    public Result userBySave(SysUser sysUser) {
        Result result = new Result();
        systemService.userBySave(sysUser);
        result.setData(sysUser);
        result.setResultCode(ResultCode.COMMON_SUCCESS);
        return result;
    }


    @PostMapping("/getMenus")
    public List<SysResource> getMenus() {
        List<SysResource> data = systemService.getMenus();
        return data;
    }

}
