package com.pake.admin.controller;

import com.alibaba.fastjson.JSONObject;
import com.lly835.bestpay.model.RefundResponse;
import com.pake.admin.common.Result;
import com.pake.admin.common.ResultCode;
import com.pake.admin.common.weChat.MpAccount;
import com.pake.admin.common.weChat.TemplateMessage;
import com.pake.admin.common.weChat.WxApiClient;
import com.pake.admin.common.weChat.WxMemoryCacheClient;
import com.pake.admin.common.weChatPay.service.IWechatPayService;
import com.pake.admin.model.TakeAway;
import com.pake.admin.service.ITakeAwayService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;


@RestController
@RequestMapping("/takeAway")
@Slf4j
public class TakeAwayCtrl {

    @Autowired
    private ITakeAwayService takeAwayService;

    @Resource
    private IWechatPayService wechatPayService;

    @GetMapping("/list")
    public Result list(
            TakeAway takeAway,
            @RequestParam(name = "pageNum", defaultValue = "1") int pageNum,
            @RequestParam(name = "pageSize",defaultValue = "10") int pageSize) {
        Result result = new Result();
        List<TakeAway> data =takeAwayService.findAll(takeAway,pageNum,pageSize);
        result.setData(data);
        result.setResultCode(ResultCode.COMMON_SUCCESS);
        return result;
    }

    @GetMapping("/getTakeAwayById")
    public Result getTakeAwayById(
            @RequestParam(name = "id") String id) {
        Result result = new Result();
        TakeAway data =takeAwayService.getTakeAwayById(id);
        result.setData(data);
        result.setResultCode(ResultCode.COMMON_SUCCESS);
        return result;
    }

    /**
     * 拒接
     *
     * */
    @PostMapping("/refuse")
    public Result refuse(
            @RequestParam(name = "id") String id,
            @RequestParam(name = "openId") String openId,
            @RequestParam(name = "refuseMessage") String refuseMessage
            ) {
        Result result = new Result();
        //1 发起退款申请
        RefundResponse refundResponse = wechatPayService.refund(id);
        log.info(refundResponse.toString());

        //2 更改订单状态
        takeAwayService.refuse(id,refuseMessage);

        //3 发送模版消息
        Executor executor = Executors.newSingleThreadExecutor();
        executor.execute(() -> {
            MpAccount mpAccount = WxMemoryCacheClient.getSingleMpAccount();
            TemplateMessage tplMsg = new TemplateMessage();
            tplMsg.setOpenid(openId);
            tplMsg.setUrl("http://www.wanxunkj.com/WeChatServer/paKe/user/userInfo");
            tplMsg.setTemplateId("h0Vy--9SBm-o63ECmtyb-ZGXSl-SBeHdk-KjrugdU0o");
            Map<String, String> dataMap = new HashMap<>();
            dataMap.put("first", "很抱歉，您的订单已被拒接\r\n");
            dataMap.put("keyword1", id + "\r\n");
            dataMap.put("keyword2", "已拒接"+ "\r\n");
            Date d = new Date();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy年MM月dd HH:mm:ss");
            String dateNowStr = sdf.format(d);
            dataMap.put("keyword3", dateNowStr + "\r\n");
            dataMap.put("remark", "拒接理由:"+refuseMessage+"\r\n"+"支付款将会按照支付渠道退回，请注意查收。感谢您选择派客配送");
            tplMsg.setDataMap(dataMap);
            JSONObject data = WxApiClient.sendTemplateMessage(tplMsg, mpAccount);
            log.info("返回结果=========>>>>>>>>"+data.toJSONString());
        });

        result.setResultCode(ResultCode.COMMON_SUCCESS);
        return result;
    }


    /**
     * 补差价
     *
     * */
    @PostMapping("/boot")
    public Result boot(
            @RequestParam(name = "id") String id,
            @RequestParam(name = "openId") String openId,
            @RequestParam(name = "bootMoney") int bootMoney,
            @RequestParam(name = "bootMessage") String bootMessage
    ) {
        Result result = new Result();
        takeAwayService.boot(id,bootMoney,bootMessage);

        //发送模版消息
        Executor executor = Executors.newSingleThreadExecutor();
        executor.execute(() -> {
            MpAccount mpAccount = WxMemoryCacheClient.getSingleMpAccount();
            TemplateMessage tplMsg = new TemplateMessage();
            tplMsg.setOpenid(openId);
            tplMsg.setUrl("http://www.wanxunkj.com/WeChatServer/paKe/user/userInfo");
            tplMsg.setTemplateId("h0Vy--9SBm-o63ECmtyb-ZGXSl-SBeHdk-KjrugdU0o");
            Map<String, String> dataMap = new HashMap<String, String>();
            dataMap.put("first", "您好，您的订单需要补价\r\n");
            dataMap.put("keyword1", id + "\r\n");
            dataMap.put("keyword2", "待补价"+ "\r\n");
            Date d = new Date();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy年MM月dd HH:mm:ss");
            String dateNowStr = sdf.format(d);
            dataMap.put("keyword3", dateNowStr + "\r\n");
            dataMap.put("remark", "感谢您选择派客配送\r\n ");
            dataMap.put("remark", "补价原因:"+bootMessage+"\r\n"+"点击详情查看更多信息。感谢您选择派客配送");
            tplMsg.setDataMap(dataMap);
            JSONObject data = WxApiClient.sendTemplateMessage(tplMsg, mpAccount);
            log.info("返回结果=========>>>>>>>>"+data.toJSONString());
        });
        result.setResultCode(ResultCode.COMMON_SUCCESS);
        return result;
    }


    /**
     * 接单
     *
     * */
    @PostMapping("/sendOut")
    public Result sendOut(
            @RequestParam(name = "id") String id,
            @RequestParam(name = "openId") String openId,
            @RequestParam(name = "disId") String disId,
            @RequestParam(name = "disName") String disName,
            @RequestParam(name = "disPhone") String disPhone
    ) {
        Result result = new Result();
        takeAwayService.sendOut(id,disId,disName,disPhone);

        //发送模版消息
        Executor executor = Executors.newSingleThreadExecutor();
        executor.execute(() -> {
            MpAccount mpAccount = WxMemoryCacheClient.getSingleMpAccount();
            TemplateMessage tplMsg = new TemplateMessage();
            tplMsg.setOpenid(openId);
            tplMsg.setUrl("http://www.wanxunkj.com/WeChatServer/paKe/user/userInfo");
            tplMsg.setTemplateId("h0Vy--9SBm-o63ECmtyb-ZGXSl-SBeHdk-KjrugdU0o");
            Map<String, String> dataMap = new HashMap<>();
            dataMap.put("first", "您好，派客已出发，请保持手机畅通\r\n");
            dataMap.put("keyword1", id + "\r\n");
            dataMap.put("keyword2", "已接单"+ "\r\n");
            Date d = new Date();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy年MM月dd HH:mm:ss");
            String dateNowStr = sdf.format(d);
            dataMap.put("keyword3", dateNowStr + "\r\n");
            dataMap.put("remark", "感谢您选择派客配送\r\n ");
            tplMsg.setDataMap(dataMap);
            JSONObject data = WxApiClient.sendTemplateMessage(tplMsg, mpAccount);
            log.info("返回结果=========>>>>>>>>"+data.toJSONString());
        });
        result.setResultCode(ResultCode.COMMON_SUCCESS);
        return result;
    }

    @GetMapping("/sendDone")
    public Result sendDone(
            @RequestParam(name = "id") String id) {
        Result result = new Result();
        takeAwayService.sendDone(id);
        result.setResultCode(ResultCode.COMMON_SUCCESS);
        return result;
    }

}