package com.pake.admin.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author Jiew
 */
@Controller
@RequestMapping("/util")
public class UtilCtrl {

    /**
     * 拉取配送员
     */
    @RequestMapping("distributor")
    public String distributor() {
        return "util/distributor";
    }

}



