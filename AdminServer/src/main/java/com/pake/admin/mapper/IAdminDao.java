package com.pake.admin.mapper;


import com.pake.admin.model.Admin;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface IAdminDao {

    Admin getAdmin(Admin admin);

    List<Map<String,Integer>> one();

    List<Map<String,String>> two();

    List<Map<String,Integer>> index();

    List<Map<String,String>> three();

    List<Map<String,String>> four();
}
