package com.pake.admin.mapper;

import com.github.pagehelper.PageInfo;
import com.pake.admin.model.TakeSend;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface ITakeSendDao {

    PageInfo findAll(TakeSend takeSend);

    TakeSend getOne(TakeSend takeSend);

    void update(TakeSend takeSend);

}
