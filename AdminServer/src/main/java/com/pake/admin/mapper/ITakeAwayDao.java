package com.pake.admin.mapper;

import com.pake.admin.model.TakeAway;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

@Mapper
public interface ITakeAwayDao {

    List<TakeAway> findAll(TakeAway takeAway);

    void update(TakeAway takeAway);

    void delete(String id);

    @Select("select id,openId, sms, note, endUserNote, endUserName, endPhone, isSubscribe, subscribeTime, state, createDate, payState, transactionId, messageTime ,disId , disName , disPhone , bootMoney , refuseMessage,bootMessage FROM pake_takeAway a where id=#{id}")
    TakeAway getTakeAwayById(@Param("id") String id);

    @Update("update pake_takeAway set state=2,disId=#{disId},disName=#{disName},disPhone=#{disPhone} where id=#{id}")
    void sendOut(@Param("id") String id,@Param("disId") String disId,@Param("disName") String disName,@Param("disPhone") String disPhone);

    @Update("update pake_takeAway set state=3 where id=#{id}")
    void sendDone(@Param("id") String id);

    @Update("update pake_takeAway set state=5,refuseMessage=#{refuseMessage} where id=#{id}")
    void refuse(@Param("id") String id, @Param("refuseMessage") String refuseMessage);

    @Update("update pake_takeAway set state=6,bootMoney=#{bootMoney},bootMessage=#{bootMessage} where id=#{id}")
    void boot(@Param("id") String id, @Param("bootMoney") int bootMoney,@Param("bootMessage") String bootMessage);
}
