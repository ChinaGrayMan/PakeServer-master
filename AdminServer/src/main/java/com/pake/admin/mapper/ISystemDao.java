package com.pake.admin.mapper;


import com.pake.admin.common.springSecurity.pojo.SysResource;
import com.pake.admin.common.springSecurity.pojo.SysRole;
import com.pake.admin.common.springSecurity.pojo.SysUser;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface ISystemDao {

    @SelectKey(statement="select max(id)+1 as num from security_role" , before=true,keyColumn = "num",keyProperty="id",resultType=Long.class)
    @Insert("INSERT INTO security_role (id, name, remarks, createDate) VALUES (#{id}, #{name},#{remarks}, now())")
    void roleBySave(SysRole sysRole);

    @Select("select id,name,remarks,createDate from security_role")
    List<SysRole> getRoles();

    @Select("select id, username, password, headerImg, createDate from security_user")
    List<SysUser> getUsers();

    @SelectKey(statement="select max(id)+1 as num from security_user" , before=true,keyColumn = "num",keyProperty="id",resultType=Long.class)
    @Insert("INSERT INTO security_user (id, username, password, headerImg, createDate) VALUES (#{id}, #{username}, #{password}, #{headerImg}, now())")
    void userBySave(SysUser sysUser);

    @Select("select id,pid,url,name,icon,sort from security_resource")
    List<SysResource> getMenus();
}
