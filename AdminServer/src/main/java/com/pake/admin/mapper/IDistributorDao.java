package com.pake.admin.mapper;

import com.pake.admin.model.Distributor;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface IDistributorDao {

    List<Distributor> getAll(Distributor distributor);

    @SelectKey(statement="select replace(uuid(),'-','') as uuid from dual" , before=true,keyColumn = "uuid",keyProperty="id",resultType=String.class)
    @Insert("INSERT INTO pake_distributor(id, idNum, name, sex, phone, idCard, state, createDate) VALUES (#{id}, #{idNum}, #{name}, #{sex}, #{phone}, #{idCard}, #{state}, now());")
    void save(Distributor distributor);

    @Update("update pake_distributor set state=1 where id = #{id}")
    void stopDis(@Param("id") String id);

    @Update("update pake_distributor set state=0 where id = #{id}")
    void runDis(@Param("id") String id);
}
