/** 全局变量 */
pageNum=1;
pageSize=10;
var rotation = function() {
    $("#goTopIcon").rotate({
        angle:0,
        duration: 3000,
        animateTo: 1080,
        callback:function(){
            $("#goTopIcon").html('<i class="material-icons md-24 text-danger"></i>');
        }
    });
};
$(window).load(function(){
    $("#loadingIco").hide();
});

$(function () {
    $(".nav").find("li").each(function () {
        var a = $(this).find("a:first")[0];
        if ($(a).attr("href") === location.pathname) {
            $(this).addClass("active");
            $(this).parent().parent().addClass("active");
        } else {
            $(this).removeClass("active");
        }
    });
    $("#goTopIcon").html('<i class="material-icons md-24 text-danger"></i>');
});

function BrowseServer() {
        var finder = new CKFinder();
        finder.basePath = '../';
        finder.selectActionFunction = SetFileField;
        finder.popup();
    }
function SetFileField(fileUrl) {
    $("#filePath").val(fileUrl);
    $("#imgShow").attr('src',fileUrl);
}



/** 异步全局配置 */
$.ajaxSetup({
    dataType: "json",
    cache: false,
    timeout: 8000,
    xhrFields: {
        withCredentials: true
    },
    error:function (XMLHttpRequest){
        switch (XMLHttpRequest.status){
            case(500):
                $("#body").load("/static/error/500.html");
                break;
            case(404):
                $("#body").load("/static/error/404.html");
                break;
            default:
                $("#body").load("/static/error/error.html");
        }
    }
});
$(document).ajaxSend(
    function () {
    $('#loadingIco').show();
    $("#goTopIcon").html('<i class="material-icons md-24 text-danger"></i>');
    rotation();
}).ajaxSuccess(
    function () {
    $("#loadingIco").hide();
    $("img").lazyload({
        effect: "fadeIn",
        threshold :500,
        placeholder : "/static/images/loading.gif"
    });
});

/** template全局配置 */
/** 计数器 */
var i = 0;
template.helper('sum', function () {
  i++;
  return i;
});

/** 返回顶部*/
function goTop(){
$('body,html').animate({scrollTop:0},500);
};

//表单序列化成JSON对象
jQuery.prototype.serializeObject=function(){  
    var obj=new Object();  
    $.each(this.serializeArray(),function(index,param){  
        if(!(param.name in obj)){  
            obj[param.name]=param.value;  
        }  
    });  
    return obj;  
};  

//字符串转日期格式
function getDate(strDate) {
	 var date = eval('new Date(' + strDate.replace(/\d+(?=-[^-]+$)/,
	  function (a) { return parseInt(a, 10) - 1; }).match(/\d+/g) + ')');
	 return date;
 }

/** 滚动显示*/
$(window).scroll(function(){
    var sc=$(window).scrollTop();
    if(sc>100){
        $("#goTop").css("display","block");
    }else{
        $("#goTop").css("display","none");
    }
});
