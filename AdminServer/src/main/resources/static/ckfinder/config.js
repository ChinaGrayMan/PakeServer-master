﻿/*
Copyright (c) 2003-2015, CKSource - Frederico Knabben. All rights reserved.
For licensing, see license.txt or http://cksource.com/ckfinder/license
*/

CKFinder.customConfig = function( config )
{
	// Define changes to default configuration here.
	// For the list of available options, check:
	// http://docs.cksource.com/ckfinder_2.x_api/symbols/CKFinder.config.html

	// Sample configuration options:
	// config.uiColor = '#BDE31E';
	// config.language = 'fr';
	// config.removePlugins = 'basket';
    // 预览区域显示内容
    config.image_previewText='';
    config.uiColor = '#f7f5f4';
    config.language = 'zh-cn';
    config.skin = 'bootstrap';
    config.removePlugins = 'basket,help';
    config.defaultSortBy = 'date';
};
